#!/usr/bin/env
# -*- coding: utf-8 -*-

import re
from pprint import pprint


class Checkout:
    """
    apples cost 50 cents, three apples cost $1.30.
    "three for a dollar," "$1.99 per pound," and "buy two, get one free."
    Our goods are priced individually. In addition, some items are multipriced: buy n of them, and they’ll
    cost you y cents. For example, item 'A' might cost 50 cents individually, but this week we have a special
    offer: buy three 'A's and they’ll cost you $1.30.
    """

    def __init__(self, pricing_rules: dict):
        """

        :param pricing_rules: listagem e regras especiais para definir preços de produtos
        """
        # Expressões regulares para extrair os preços e suas regras
        self.offer_regex_rules = {
            0: {'operation': 'division', 'regex': '([0-9]+) for ([0-9]+)'}
        }

        self.pricing_rules = pricing_rules
        # itens da compra
        self.checkout_items = {}

    def scan(self, item):
        """
        Lê o item sendo comprado
        :param item:
        :return:
        """
        price_rule = self.pricing_rules.get(item)
        if not price_rule:
            raise ValueError('I do not know the item %s', item)

        if price_rule.get('name') != item and price_rule.get('price', 0) == 0:
            raise ValueError('Item "%s" is not correct' % item)

        # atualiza os itens da compra
        if item not in self.checkout_items.keys():
            self.checkout_items.update({item: {'name': item, 'quantity': 1, 'quantity_processed': 0}})
        else:
            self.checkout_items[item]['quantity'] += 1

        price = price_rule.get('price')
        special_offer = price_rule.get('special_price')  # special_price não é um bom nome
        final_price = self.get_item_price_by_special_offer(price_rule) if special_offer else price
        self.checkout_items[item]['price'] = final_price

    def total(self):
        """
        Retorna o total da compra
        :return:
        """
        total = 0
        #  pprint(self.checkout_items)
        for key, item in self.checkout_items.items():
            total += item.get('price', 0)

        return int(total)

    def get_item_price_by_special_offer(self, price_rule: dict):
        item = price_rule.get('name')
        special_offer = price_rule.get('special_price')
        price = float(price_rule.get('price'))
        quantity_remaining = int(self.checkout_items.get(item).get('quantity')) - int(self.checkout_items.get(item).get('quantity_processed'))
        final = 0

        # pprint(price_rule)
        for key, offer in self.offer_regex_rules.items():
            match = re.match(offer.get('regex'), special_offer, re.IGNORECASE)
            if match:
                if offer.get('operation') == 'division':
                    #print('bateu! operacao eh %s, quantidade eh %s e price eh %s' % (offer.get('operation'), quantity, price))
                    # print("matchs ", match.groups())
                    #print("match0 %s match1 %s" % (match.group(1), match.group(2)))

                    quantos = int(match.group(1))
                    por = float(match.group(2))

                    if quantity_remaining >= quantos:
                        q = (quantity_remaining / quantos)
                        # print("q ", q)
                        sobrou = quantity_remaining % quantos
                        # print("sobrou ", sobrou)
                        final = (q * por) + (sobrou * price)
                        quantity_remaining -= quantos
                    else:
                        final = price * quantity_remaining

        # print("final ", final)
        # print()
        return final
