import unittest
from checkout.checkout import Checkout


class TestCheckout(unittest.TestCase):
    def setUp(self):
        self.RULES = {
            'A': {'name': 'A', 'price': 50, 'special_price': '3 for 130'},
            'B': {'name': 'B', 'price': 30, 'special_price': '2 for 45'},
            'C': {'name': 'C', 'price': 20, 'special_price': ''},
            'D': {'name': 'D', 'price': 15, 'special_price': None},
        }

    def price(self, items):
        co = Checkout(self.RULES)

        for item in items:
            co.scan(item)

        return co.total()

    def test_totals(self):
        self.assertEqual(0, self.price(''))
        self.assertEqual(50, self.price('A'))
        self.assertEqual(80, self.price('AB'))
        self.assertEqual(115, self.price('CDBA'))

        self.assertEqual(100, self.price('AA'))
        self.assertEqual(130, self.price('AAA'))
        self.assertEqual(180, self.price('AAAA'))
        self.assertEqual(230, self.price('AAAAA'))
        self.assertEqual(260, self.price('AAAAAA'))

        self.assertEqual(160, self.price('AAAB'))
        self.assertEqual(175, self.price('AAABB'))
        self.assertEqual(190, self.price('AAABBD'))
        self.assertEqual(190, self.price('DABABA'))

    def test_incremental(self):
        co = Checkout(self.RULES)
        self.assertEqual(0, co.total())
        co.scan('A')
        self.assertEqual(50, co.total())
        co.scan('B')
        self.assertEqual(80, co.total())
        co.scan('A')
        self.assertEqual(130, co.total())
        co.scan('A')
        self.assertEqual(160, co.total())
        co.scan('B')
        self.assertEqual(175, co.total())

if __name__ == '__main__':
    unittest.main()
