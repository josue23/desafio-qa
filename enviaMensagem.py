from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.common.keys import Keys
import time

# configurações
tentativas = 5
mensagem = 'Hi! are you there?'

# Selenium web drivers
driver = None

def aguarda(tempo_aguarda=5):
	time.sleep(tempo_aguarda) # aguarda 5 segundos

# Carrega o driver para firefox
def web_driver_load():
	driver = webdriver.Firefox()

# Fecha o driver
def web_driver_quit():
	driver.quit()

# Faz login no whatsapp web
def wpp_login():
	driver.get('https://web.whatsapp.com/');
	aguarda(10) # aguarda dez segundos

# envia a mensagem
msg = input('Digite a mensagem: ')
def enviaMensagem(msg):
        # encontra o campo a ser preenchido
	web_obj = driver.find_element_by_xpath("//div[@contenteditable='true']")
	web_obj.send_keys(msg) # preenche o campo da mensagem
	web_obj.send_keys(Keys.RETURN) # envia a mensagem


# Método principal
if __name__ == "__main__":
	web_driver_load()
	wpp_login()
	aguarda()
	
	for i in range(tentativas):
		enviaMensagem(mensagem) # envia a mensagem n vezes
		aguarda()

	aguarda()
	web_driver_quit()
